## Installation
- Create new project folder<br>
- Move to folder and init npm:<br>
    `npm init`<br>
  Set name for project and skip others param settings<br>
- Install express module:<br>
    `npm install express --save`<br>
- Create file app.js and paste this bellow starter script:<br>

```javascript
    var express = require('express');
    var app = express();
    
    app.get('/', function (req, res) {
      res.send('Hello World!');
    });
    
    app.listen(3000, function () {
      console.log('Example app listening on port 3000!');
    });
```

- Start app.js with this command:<br>
    `node app.js`<br>
- Open browser:<br>
    `http://localhost:3000`


## Using Template Engine
- Install pug package:<br>
    `npm install pug --save`<br>
- Paste this line to app.js. Make sure to put it in config or above all routing scripts:<br>

```javascript
    app.set('view engine', 'pug')
```

- Create folder views, and create a file in it with below script and save it:<br>

```javascript
    html
      head
        title= title
      body
        h1= message
```

- Change hello world's line with this script:<br>

```javascript
    res.render('index', { title: 'Hey', message: 'Hello there!' })
```

- Check the changes in browser

## Create simple API by reading json file
- Create json file and paste this json object:<br>

```javascript
    [
        {
            "firstname": "max",
    		"lastname": "musterman",
    		"email": "max@email.com"
        },
        {
            "firstname": "myfirstname",
    		"lastname": "mylastname",
    		"email": "myfirstname@email.com"
        }
    ]
```

- Install fs package: <br>
    `npm install fs --save`<br>

- Insert this script to app.js:<br>

```javascript
    var fs = require("fs");
    ...
    ...
    app.get('/api/users', function (req, res){
        var filepath = 'file.json';
        var file = fs.readFileSync(filepath, 'utf8');
        res.send(JSON.parse(file));
    })
```

- Open this url in browser and you will see your first objects from a file:<br>
    `http://localhost:3000/api/users`