var express = require('express');
var app = express();
var fs = require("fs");

app.listen(3000, function () {
    console.log('Example app listening on port 3000!');
});

// app.set('view engine', 'pug')
app.set('view engine', 'ejs')

app.get('/', function (req, res) {
    res.render('index', { title: 'Title of Page' , message: 'Hello there!' })
})

app.get('/api/users', function (req, res){
    var filepath = 'file.json';
    var file = fs.readFileSync(filepath, 'utf8');
    res.send(JSON.parse(file));
})